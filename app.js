let express = require('express');
let config = require('./config');
let path = require('path');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let moment = require('moment');
let index = require('./routes/index');
let users = require('./routes/users');
let login = require('./routes/login');
let mongoose = require('mongoose');
let app = express();

mongoose.connect(config.connect_db);
moment.locale('en');

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Acc" +
      "ess-Control-Request-Method, Username, Password, Access-Control-Request-Headers");
  next();
});

// view engine setup app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade'); uncomment after placing your favicon in
// /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/login', login);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req
    .app
    .get('env') === 'development'
    ? err
    : {};
  res.status(200);
  res.json({
    'statusCode': 404,
    'dateTime': moment().format('LLLL')
  })
});

module.exports = app;