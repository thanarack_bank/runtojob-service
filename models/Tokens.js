const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SchemaData = {
  app: String,
  id_user: String,
  active: String,
  time_out: String,
  time_out_refresh: String,
  scope: String,
  token: String,
  refresh_token: String,
  createAt: Date,
  updateAt: Date
};
const Modal = new Schema(SchemaData);
const TokenTable = mongoose.model('tokens', Modal);
module.exports = TokenTable;