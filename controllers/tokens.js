const tokensTable = require('../models/Tokens');
const config = require('../config');
const jwt = require('jsonwebtoken');

const token_save = (uid, scope, token, refresh_token) => {
  let time_out = jwt.verify(token, config.jwt_key_secret, function (err, decoded) {
    return decoded.exp;
  });
  let time_out_refresh = jwt.verify(refresh_token, config.jwt_key_secret, function (err, decoded) {
    return decoded.exp;
  });
  const save_data = {
    app: config.app_id,
    id_user: uid,
    active: 'Active',
    scope: scope,
    time_out: time_out,
    time_out_refresh: time_out_refresh,
    token: token,
    refresh_token: refresh_token,
    createAt: new Date(),
    updateAt: new Date()
  }
  const create_token = new tokensTable(save_data);
  return create_token.save();
}

module.exports = {
  token_save
}