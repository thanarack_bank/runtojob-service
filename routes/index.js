let express = require('express');
let router = express.Router();
let home = require('../controllers/home');
let moment = require('moment');

/* GET home page. */
router.get('/',
  home.homecon,
  home.homecon2(665),
  function (req, res, next) {
    res.json({
      'statusCode': 404,
      'dateTime': moment().format('LLLL')
    })
  });

module.exports = router;
