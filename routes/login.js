const express = require('express');
const router = express.Router();
const con_login = require('../controllers/login');

router.post('/', [
  con_login.headerLoginCheck, con_login.generateTokenUser
], (req, res, next) => {
  res
    .status(200)
    .json({'statusCode': 200, 'message': 'Login success', 'token': req._token, 'refresh_token': req.refresh_token});
});

router.post('/create_user', [
  con_login.headerLoginCheck, con_login.createUser
], (req, res, next) => {
  res
    .status(200)
    .json({'statusCode': 200, 'message': 'User created'});
});

module.exports = router;